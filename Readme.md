# Simmons (Simple Routine Monitors)

Simmons is a simple monitoring library for go, which allows the tracking of go 
routines. A monitor of simmons can pass err values and log messages to a root 
monitor. A monitor has three main methods, which are Done(logMessage), Error(error)
and Log(logMessage). Done singles an parent monitor, that the end of the go routine
was reached. While Error reports that an error has occurred. Log is used to log 
additional information, like for example warnings.

Monitors could create sub-monitors, which allow the monitoring of sub-routines.
The parent monitor needs to wait until all sub-monitors have finished. This can
be done via the Wait() method of the parent monitor. If multiple monitors are
started on the same level, they need to be re-joined over the method Join() before
the sub monitor can return to an higher monitor.

There are also the methods Hold() and Go() which can be used to stop a parent
monitor after the methods Done or Error were called. If you want to know more
look at the example withUnmonitoredSubRoutine.

An example for a basic monitor with Wait() can be seen below.

For more examples looks into the examples subfolder.

## Installation

Simmons can be installed over the go get command line tool.

```
    go get gitlab.com/joleus/simmons
```

import it into your go program with the following import line.

```go
    import monitor "gitlab.com/joleus/simmons"
```

## Example with Wait()

An classic example would be the WithWait example from the example package.

Here a root monitor is created and placed into the subprocess (go routine),
that it should monitor. Monitors could have names, which should help to 
identify them later on. The top monitor is normally called root, while the 
others are mostly named after the procedures they monitor.

After this the root process need to Wait() until all sub processes (only one 
in this case) are Done(). After the waiting, the root monitor receives the data
from the sub monitors and prints the error and change logs out, over the internal
printRoot function.

```go
    func WithWait(withTime bool) {
        fmt.Println("With Wait")

        /* creates root monitor. Every thing is passed here. */
        root := monitor.NewRoot("root", false)
        go subProcess(root)

        /* waits until every sub monitor finished */
        root.Wait()

        /* prints the changes and errors*/
        printRoot(root, withTime)
    }

    func subProcess(m *monitor.Monitor) {

        fmt.Println("Sublevel")

        if r := rand.Intn(2); r == 0 {
            m.ErrorF("Error in Sublevel")
        } else {
            m.Done("Done with Sublevel")
        }
    }
```

## License

Simmons is released under the MIT license. See [LICENSE](LICENSE)

## Why the Name Simmons

The name Simmons is portmanteau or blend word of **Sim**ple and **Mon**itor**s**.
Okay that doesn't explain why I choose the name, but does that circumstance really matter.

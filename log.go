package monitor

/*
MIT License
Copyright (c) 2024 Jens Ohlmann
see Readme.md for reference
*/

import "time"

type ChangeLog interface {
	/* Changes returns the changes as string. */
	Changes() string
	/* Time returns the time the change appread */
	Time() time.Time
}

type _log struct {
	msg  string
	time time.Time
}

func (l *_log) Changes() string {
	return l.msg
}

func (l *_log) Time() time.Time {
	return l.time
}

func NewLog(changes string) ChangeLog {
	return &_log{msg: changes, time: time.Now()}
}

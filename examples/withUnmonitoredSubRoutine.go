package examples

import (
	"fmt"
	"math/rand"
	"time"

	monitor "gitlab.com/joleus/simmons"
)

/*
In this example subprocess two is monitored, while subprocess one
isn't. Which process it monitored is determined by the Done() method.

Subprocess one could still ask over the Err() method, if subprocess
two ended successful. If subprocess two didn't end successfully it
can deterime this with the Err() method of the monitor and end it's
procedure with a warning log or continue with a clean up operation.

It needs to be noted, that in the clean up case, the parent monitor
needs to be set to hold via the Hold() method or the operation would
otherwise have the possibility to be stopped harshly via the runtime.
This could happen because the parent function continuous execution.
*/
func WithUnmonitoredSubRoutine(withTime bool) {
	fmt.Println("With Unmonitored Sub Routine")

	/* creates root monitor. Every thing is passed to here. */
	root := monitor.NewRoot("root", false)
	go processOne(root)

	/* waits until every sub monitor finished */
	root.Wait()

	/* prints the changes and errors*/
	printRoot(root, withTime)
}

/* subProcessOne isn't monitored. It doesn't calls the done method. */
func subProcessOne(m *monitor.Monitor) {

	subProcessTwo(m)
	/* if sub process returns err,
	end this process with a warning log*/
	if m.Err() != nil {
		m.Log("Warning with 1")
		return
	}
	/* hold parent monitor from executing.
	Important because otherwise the program
	would end.*/
	m.Hold()
	/* perform clean up operation

	the time.Sleep() represents an operation
	that lead to a go routine switch.*/
	time.Sleep(1 * time.Second)
	m.Log("Log with 1")
	m.Go()

}

func subProcessTwo(m *monitor.Monitor) {

	if r := rand.Intn(2); r == 0 {
		m.ErrorF("Error in 2")
	} else {
		m.Done("Done with 2")
	}
}

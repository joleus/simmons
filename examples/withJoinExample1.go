package examples

import (
	"fmt"
	"math/rand"

	monitor "gitlab.com/joleus/simmons"
)

/*
In this example two sub processes are started continuously via
the go keyword. The sub processes are called root1 and root2, because
the method join can only join root monitors. Join waits until all
monitors are joined and then adds the logs and errors to the root
monitor. The root monitor is then printed.

Not that here, there is no Wait(), because the root monitor
doesn't need to Wait() any longer after the join operation.
Look at withJoinExample2 to see an example which contains
Wait(), Hold() and Go().
*/
func WithJoinExample1(withTime bool) {

	fmt.Println("With Join Example 1")
	/* creates root monitor. Every thing is passed here. */
	root := monitor.NewRoot("root", false)
	i := 0
	j := 0
	/* creates sub monitors. Need to root for join */
	root1 := monitor.NewRoot("subProcess1", false)
	root2 := monitor.NewRoot("subProcess2", false)

	/* starts processes */
	go process(root1, &i)
	go process(root2, &j)

	/* joins monitors after return */
	root.Join("", root1, root2)

	/* prints the changes and errors*/
	printRoot(root, withTime)

}

func process(mon *monitor.Monitor, i *int) {
	*i++
	processID := *i

	r := rand.Intn(2)
	/* r := 0 */
	if *i < 5 {
		/* subprocess */
		sub := mon.NewMonitor(mon.Name())
		go process(sub, i)
		if err := sub.Wait(); err != nil {
			return
		}
	}
	verbal := ""
	if r == 1 { // if true error
		/* Sending */
		mon.ErrorF("Error at %v of %v", processID, mon.Name())
		verbal = "had error"
	} else {
		/* Sending */
		mon.DoneF("Changed at %v of %v|", processID, mon.Name())
		verbal = "is done"
	}

	fmt.Printf("process %v of %v %v\n", processID, mon.Name(), verbal)
}

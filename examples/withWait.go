package examples

import (
	"fmt"
	"math/rand"

	monitor "gitlab.com/joleus/simmons"
)

/*
An classic example would be the WithWait example from the example package.

Here a root monitor is created and placed into the subprocess (go routine),
that it should monitor. Monitors could have names, which should help to
identify them later on. The top monitor is normally called root, while the
others are mostly named after the procedures they monitor.

After this the root process need to Wait() until all sub processes (only one
in this case) are Done(). After the waiting, the root monitor receives the data
from the sub monitors and prints the error and change logs out, over the internal
printRoot function.
*/
func WithWait(withTime bool) {
	fmt.Println("With Wait")

	/* creates root monitor. Every thing is passed here. */
	root := monitor.NewRoot("root", false)
	go subProcess(root)

	/* waits until every sub monitor finished */
	root.Wait()

	/* prints the changes and errors*/
	printRoot(root, withTime)
}

func subProcess(m *monitor.Monitor) {

	fmt.Println("Sublevel")

	if r := rand.Intn(2); r == 0 {
		m.ErrorF("Error in Sublevel")
	} else {
		m.Done("Done with Sublevel")
	}
}

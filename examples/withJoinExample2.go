package examples

import (
	"fmt"
	"time"

	monitor "gitlab.com/joleus/simmons"
)

/*
This example shows an alternative approach to the
Join() method which builds up on the approach seen
in withJoinExample1. Go there for reference.
*/
func WithJoinExample2(withTime bool) {

	fmt.Println("With Join Example 2")
	/* creates root monitor. Every thing is passed here. */
	root := monitor.NewRoot("root", false)

	go StartSubProcesses(root)

	root.Wait()
	/* prints the changes and errors*/
	printRoot(root, withTime)

}

func StartSubProcesses(m *monitor.Monitor) {
	/* ProcessIDs */
	i := 0
	j := 0
	/* creates sub monitors. Need to be root for join */
	root1 := monitor.NewRoot("subProcess1", false)
	root2 := monitor.NewRoot("subProcess2", false)

	/* starts processes */
	go process(root1, &i)
	go process(root2, &j)
	/* joins monitors after return */
	if m.Join("", root1, root2) != nil {
		m.Error(m.Err())
		m.Hold()
		/* Performs clean up operation */
		time.Sleep(1 * time.Second)
		m.Log("Clean up done")
		m.Go()
		return
	}
	m.Done("All subprocesses joined")

}

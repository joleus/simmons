package examples

import (
	"fmt"

	monitor "gitlab.com/joleus/simmons"
)

/* printRoot prints the root monitors changes logs and errors */
func printRoot(root *monitor.Monitor, withTime bool) {
	fmt.Printf("root.err: %v\n", root.Err())
	logString := ""
	for _, log := range root.Logs() {
		if withTime {
			logString = logString + fmt.Sprintf("%v at %v", log.Changes(), log.Time())
		} else {
			logString = logString + fmt.Sprintf("%v ", log.Changes())
		}

	}
	fmt.Printf("log: %v\n", logString)
}

package examples

import (
	"fmt"
	"math/rand"

	monitor "gitlab.com/joleus/simmons"
)

/*
In this example processOne and processTwo are monitored via a
sub monitor. The monitor of processTwo is a sub monitor from
the monitor of processOne, while it's monitor is a sub monitor
from root. The lowest sub monitor first sends it's data to his
parent, which is the sub monitor of procedureOne. Then it checks
if the process returned an error and if not continoues with its
own operation.

If an sub process don't need an own operator. An unmonitored
sub routine could be created. A sub routine is called unmonitored
if it doesn't calls the done method. If this area still needs to
perform an action, its parent monitor could be set to hold, via
the Hold() method of the monitor.
*/
func WithSubRoutines(withTime bool) {

	fmt.Println("With Sub Routines")

	/* creates root monitor. Every thing is passed to here. */
	root := monitor.NewRoot("root", false)
	go processOne(root)

	/* waits until every sub monitor finished */
	root.Wait()

	/* prints the changes and errors*/
	printRoot(root, withTime)
}

/* process one ends if process two has an error*/
func processOne(m *monitor.Monitor) {
	sub := m.NewMonitor("")
	go processTwo(sub)

	/* waits until process one has returned */
	if sub.Wait() != nil {
		return
	}

	/* this code is processed after all monitors returned */
	fmt.Println("Sub Routines 1")

	if r := rand.Intn(2); r == 0 {
		m.ErrorF("Error in 1")
	} else {
		m.Done("Done with 1")
	}
}

func processTwo(m *monitor.Monitor) {

	fmt.Println("Sub Routines 2")

	if r := rand.Intn(2); r == 0 {
		m.ErrorF("Error in 2")
	} else {
		m.Done("Done with 2")
	}
}

package monitor

/*
MIT License
Copyright (c) 2024 Jens Ohlmann
see Readme.md for reference
*/

import "fmt"

/*
Monitor represents a routine/procedure monitor. Monitors
could be used together with go routines, to accumulate meta data
of the procedure. They can handle errors as well as documenting
the changes that operations performed. There are two types of monitors.
Root monitors are the root of every monitoring system, because every
other non-root monitor returns there data to them. Sub-Monitors are
monitors that are created form other monitors (root or sub) to oversee
a sub-procedure (go-routine) for them.
*/
type Monitor struct {
	name string
	/* isRoot is used to determine if this monitor
	is a root monitor. It can't be done over name,
	even though only root monitors could have names,
	because that's not unambiguous enough. */
	isRoot    bool
	noLogging bool
	errChan   chan error
	logs      []ChangeLog
	err       error
	wantsHold bool
	holdChan  chan bool
	parent    *Monitor
}

/* Err() returns the error value of the monitor. Nil if no error. */
func (m *Monitor) Err() error {
	return m.err
}

/* Logs returns the ChangeLog of the monitor, Nil if the monitor contains no change logs */
func (m *Monitor) Logs() []ChangeLog {
	return m.logs
}

/* Name returns the name of the monitor, empty string ("") if left blank */
func (m *Monitor) Name() string {
	return m.name
}

/*
The method Error() could be called in a sub-context, to signal
the root-context, that an error has been occurred. The sub-context
should end closely (clean up are desired of course), after the
method Error is called. The function passes the error with the
error method to the root context.
*/
func (m *Monitor) Error(err error) {
	m.err = err
	m.errChan <- err
}

/*
ErrorF is the same as the method Error(), but it allows an
formatted string as input.
*/
func (m *Monitor) ErrorF(format string, a ...any) {
	m.Error(fmt.Errorf(format, a...))
}

/*
DoneF is the same as the method Done(), but it allows an
formatted string as input.
*/
func (m *Monitor) DoneF(format string, a ...any) {
	m.Done(fmt.Sprintf(format, a...))
}

/*
DoneL is the same as the method done(), but it allows an
an own changeLog struct.
*/
func (m *Monitor) DoneL(log ChangeLog) {
	m._done(log)
}

/*
The method Done() could be called in a sub-context, to signal
the root-context, that the operation is done. A description of
the operation could be passed as message for the ChangeLog.
The Done method is normally run at the end of a procedure.
*/
func (m *Monitor) Done(logMessage string) {
	m._done(NewLog(logMessage))
}

/* internal done method */
func (m *Monitor) _done(log ChangeLog) {
	if !m.noLogging {
		m.logs = append(m.logs, log)
	}
	m.errChan <- nil
}

/*
LogF is the same as the method Log(), but it allows an
formatted string as input.
*/
func (m *Monitor) LogF(format string, a ...any) {
	m.Log(fmt.Sprintf(format, a...))
}

/*
LogL is the same as the method Log(), but it allows an
an own changeLog struct.
*/
func (m *Monitor) LogL(log ChangeLog) {
	m._log(log)
}

/*
Log creates an additional log message, which could for example
be used, to log a warring. Log is basically the method Done(),
but without signaling the parent monitor, that the method is
done.
*/
func (m *Monitor) Log(logMessage string) {
	m._log(NewLog(logMessage))
}

/* internal log method */
func (m *Monitor) _log(log ChangeLog) {
	if !m.noLogging {
		m.logs = append(m.logs, log)
	}
}

/*
NewMonitor creates a new sub-monitor. Sub-Monitors
return to there parent monitor if method Done() or Error()
are called. Monitors could contain a name as reference,
which could help to identify them. The name of the monitor
is normally related to the subprocess description which it
is monitoring.
*/
func (m *Monitor) NewMonitor(name string) *Monitor {
	mon := Monitor{name: name, noLogging: m.noLogging, errChan: make(chan error), holdChan: make(chan bool), parent: m}
	return &mon
}

/*
Wait lets an monitor wait until all of its sub-monitors have
returned. Call this function after a go routine has been
called to stop execution of the current context and let
the sub-context proceed. It automatically adds the changes
of all successfully ended sub-procedures to the ChangeLog of
the monitor. If an error occurred, the Wait method returns
the error as output and automatically sends the error signal
to a higher monitor. The context than needs to be closed
with a return statement to end the sub-context.
*/
func (m *Monitor) Wait() error {
	m.err = <-m.errChan

	if m.wantsHold {
		<-m.holdChan
	}

	/* if no error (nil) and parent isn't root.
	Write changes to the next higher monitor.*/
	if m.parent != nil {
		m.parent.logs = append(m.parent.logs, m.logs...)
		m.parent.err = m.err
		if m.err != nil {
			m.parent.errChan <- m.err
		}
	}
	return m.err

}

/*
The method Join() allows to rejoin multiple root monitors together.
It waits until all sub-monitors of the root monitors have finished
(either Done() or Error()) and then adds there log and combines there
error messages together. Note, that the input to this method need to
be a set of root monitors.

If multiple root monitors received errors, the errors are joined
together as one continuous error. An own errorFormat can be specified
via the errorFormat parameter. The default error format and it's
result, is shown below. If the error format is left empty or
don't contain two placeholders (%v), it's seen to be invalid
an replaced by the default error format.

defaultErrorFormat = "monitor %v returned error (%v) : "

defaultError =  "monitor 1 returned error (error 1) : monitor 2 returned error (error 2)"
*/
func (m *Monitor) Join(errorFormat string, roots ...*Monitor) error {

	/* validate input */
	countFormats := 0
	for _, char := range errorFormat {
		if char == '%' {
			countFormats++
		}
	}

	if countFormats != 2 {
		errorFormat = "monitor %v returned error (%v) : "
	}

	for _, root := range roots {
		if !root.isRoot {
			panic(fmt.Errorf("method join requires root monitors and %v isn't a root monitor", root.name))
		}
	}

	/* perform join operation */
	var errorMsg string
	rootLength := len(roots)
	hitted := make([]bool, rootLength)
	i := 0
	for {
		if !hitted[i] {
			root := roots[i]
			err := <-root.errChan
			hitted[i] = true
			if !m.noLogging {
				m.logs = append(m.logs, root.logs...)
			}
			if err != nil {
				errorMsg = errorMsg + fmt.Sprintf(errorFormat, root.name, err)
			}
		}
		/* loops as long until all monitors have returned */
		i++
		if i == rootLength {
			i = 0
		}

		/* check if all are hitted */
		check := true
		for _, hit := range hitted {
			if !hit {
				check = false
			}
		}
		if check {
			break
		}

	}
	/* Set error */
	if errorMsg == "" {
		m.err = nil
	} else {
		m.err = fmt.Errorf(errorMsg)
	}
	return m.err

}

/*
The method Hold() allows to hold an parent monitor from continuing.
It could be called after a Done() to stop the parent for a bit longer.
The hold can be released by calling the Go() method of the monitor.
*/
func (m *Monitor) Hold() {
	m.wantsHold = true
}

/* The Go() method is part of the Hold() method. Look there for reference. */
func (m *Monitor) Go() {
	m.holdChan <- true
}

/*
NewRoot creates a new root monitor. Monitors could contain
a name as reference, which could help to identify them.
The highest root monitor is normally called root. The name
of the monitor is normally related to the subprocess
description which it is monitoring. Root monitors are
normally used if multiple sub processes are started in
one context.
*/
func NewRoot(name string, noLogging bool) *Monitor {
	return &Monitor{name: name, isRoot: true, noLogging: noLogging, errChan: make(chan error), holdChan: make(chan bool), parent: nil}

	/* but whats with sub processes */
}
